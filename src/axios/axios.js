import axios from 'axios'

async function getApi() {
    const pet = await axios.get("https://0q27loouph.execute-api.us-east-1.amazonaws.com/")

    return pet.data
}

export default getApi
