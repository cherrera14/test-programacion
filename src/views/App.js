import getApi from "../axios/axios"

import {Table, Button} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.css'
import React, {useState, useEffect} from 'react';


function App() {

    const getData = async () => {
        const info = await getApi()
        setInfo(info)
    }
    const [info, setInfo] = useState(() => getData())

    return (
        <div>
            <div className="row justify-content-center">
                <Table className="w-50" bordered striped>
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Image</th>
                        <th>Nutritionist</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{info.name}</td>
                        <td>{info.email}</td>
                        <td>{info.phone}</td>
                        <td><img width="100" height="100" src={info.image}/></td>
                        <td>{info.nutritionist}</td>
                    </tr>
                    </tbody>
                </Table>
            </div>
            <div className={"row justify-content-center"}>
                <Button variant="primary" size={"sm"} onClick={() => getData()}>Update</Button>
            </div>

        </div>
    );
}

export default App;
